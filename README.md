# kafka-python-client
## To use
Start up Zookeeper
```
$ bin/zookeeper-server-start.sh config/zookeeper.properties
```
Start up Kafka
```
$ bin/kafka-server-start.sh config/server.properties
```
Start the Kafka producer, assumes topic is *test*
```
$ bin/kafka-console-producer.sh --broker-list 192.168.2.9092 --topic test
```
Run the kafka-python-consumer.py
```
./kafka-python-consumer.py -b broker_host:9092 -t test
```
## For Help
```
./kafka-python-consumer.py --help
```
