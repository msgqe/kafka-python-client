#!/usr/bin/env python

"""
A consumer for kafka written in python.
extend this consumer and leverage for testing.

Author @esammons
"""

from kafka import KafkaConsumer
from optparse import OptionParser
import os
import sys


def consume(brokers, topic):
    """Consume messages off a kafka broker's topic.

    Args:
        brokers: A list of brokers w/ ports.
        topic: The topic to consume from.
    """
    consumer = KafkaConsumer(topic,
                             bootstrap_servers=brokers,
                             session_timeout_ms=6000,
                             )
    try:
        for msg in consumer:
            print(msg)
    except KeyboardInterrupt:
        sys.stderr.write('\nConsumer aborted by user\n')
    consumer.close()


if __name__ == "__main__":
    """Setup the Kafka consumer

    Returns:
        True for success, False otherwise.
    """

    kafka_brokers = os.environ.get("BROKERS", None)
    kafka_topic = os.environ.get("TOPIC", None)

    parser = OptionParser()
    parser.add_option("-t", "--topic", dest="kafka_topic",
                      help="Kafka topic to consume", metavar="TOPIC",
                      default=kafka_topic)
    parser.add_option("-b", "--brokers", dest="kafka_brokers",
                      help="Kafka brokers, comma separated",
                      metavar="host1:port, host2:port",
                      default=kafka_brokers)

# =============================================================================
#  parser.add_option("-k", "--key", dest="kafka_key",
#                    help="Kafka key to be used", metavar="KEY", default="")
# =============================================================================

    (opts, args) = parser.parse_args()

    # =============================================================================
    #  consume(opts.kafka_brokers.split(','), opts.kafka_topic, opts.kafka_key)
    # =============================================================================
    consume(opts.kafka_brokers.split(','), opts.kafka_topic)
